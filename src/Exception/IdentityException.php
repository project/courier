<?php

namespace Drupal\courier\Exception;

/**
 * Defines an exception for when an identity cannot be applied.
 */
class IdentityException extends \Exception {}
