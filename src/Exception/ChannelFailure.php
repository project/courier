<?php

namespace Drupal\courier\Exception;

/**
 * Defines an exception for when a message cannot be sent.
 */
class ChannelFailure extends \Exception {}
